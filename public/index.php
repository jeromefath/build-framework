<?php declare(strict_types=1);

require '../vendor/autoload.php';

$request = Laminas\Diactoros\ServerRequestFactory::fromGlobals(
    $_SERVER, $_GET, $_POST, $_COOKIE, $_FILES
);

$router = new League\Route\Router;
$router->map('GET', '/', 'Controller\DefaultController::index');
$router->map('GET', '/show', 'Controller\DefaultController::show');

try {
    $response = $router->dispatch($request);
}
catch (\League\Route\Http\Exception\NotFoundException $notFoundException) {
    $response = new \Laminas\Diactoros\Response\TextResponse(
        $notFoundException->getMessage(), 404
    );
}

$emitter = new Laminas\HttpHandlerRunner\Emitter\SapiEmitter();
$emitter->emit($response);