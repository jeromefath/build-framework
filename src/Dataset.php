<?php

declare(strict_types=1);

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Model\Area;
use Model\County;
use Symfony\Component\HttpClient\HttpClient;

/**
 * Class Dataset
 *
 * @author Jérôme Fath
 */
final class Dataset
{
    /** @var Collection|Area[] */
    private $areas;

    public function __construct()
    {
        $this->areas = new ArrayCollection();

        $this->load();
    }

    public function getAreas(): array
    {
        return $this->areas->toArray();
    }

    public function getArea(string $code): ?Area
    {
        foreach ($this->areas as $area) {
            if ($code === $area->getCode()) {
                return $area;
            }
        }
        return null;
    }

    public function getCounty(string $code): ?County
    {
        foreach ($this->areas as $area) {
            $county = $area->getCounty($code);
            if (null !== $county) {
                return $county;
            }
        }
        return null;
    }

    private function load(): void
    {
        $datas = HttpClient::create()
            ->request('GET', 'https://www.data.gouv.fr/fr/datasets/r/b4637758-1184-497c-b8ea-a24d456163d0')
            ->toArray()
        ;

        foreach ($datas as $data) {
            $this->populate($data['fields']);
        }
    }

    private function populate(array $data): void
    {
        $area = $this->getArea($data['code_region']);
        if (null === $area) {
            $area = new Area();
            $this->areas->add($area);
        }
        $area->update($data);
    }
}
