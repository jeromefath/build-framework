<?php declare(strict_types=1);

namespace Controller;

use Dataset;
use Laminas\Diactoros\Response;
use League\Route\Http\Exception\NotFoundException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use League\Plates\Engine;

/**
 * Class DefaultController
 *
 * @author Jérôme Fath
 */
final class DefaultController
{
    /** @var Engine */
    private $engine;

    /** @var Dataset */
    private $dataset;

    public function __construct()
    {
        $this->engine = new Engine(__DIR__ . '/../../views/');
        $this->dataset = new Dataset();
    }

    public function index(ServerRequestInterface $request): ResponseInterface
    {
        $content = $this->engine->render('default/index', [
            'title' => 'Régions de France',
            'areas' => $this->dataset->getAreas()
        ]);

        return $this->createResponse($content);
    }

    public function show(ServerRequestInterface $request): ResponseInterface
    {
        $queryParams = $request->getQueryParams();
        if (empty($queryParams['county'])) {
            throw new NotFoundException();
        }

        $county = $this->dataset->getCounty($queryParams['county']);
        if (null === $county) {
            throw new NotFoundException(sprintf('County with code "%s" not found', $queryParams['county']));
        }

        $content = $this->engine->render('default/show', [
            'title' => $county->getName(),
            'county' => $county
        ]);

        return $this->createResponse($content);
    }

    private function createResponse(string $content): ResponseInterface
    {
        $response = new Response() ;
        $response->getBody()->write($content);

        return $response;
    }
}
