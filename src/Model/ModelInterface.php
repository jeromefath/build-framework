<?php

declare(strict_types=1);

namespace Model;

/**
 * Interface ModelInterface
 *
 * @author Jérôme Fath
 */
interface ModelInterface
{
    public function update(array $data): self;
}
