<?php

declare(strict_types=1);

namespace Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Class Area
 *
 * @author Jérôme Fath
 */
class Area implements ModelInterface
{
    /** @var string */
    protected $name;

    /** @var string */
    protected $code;

    /** @var Collection|County[] */
    protected $counties;

    public function __construct()
    {
        $this->counties = new ArrayCollection();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function addCounty(County $county)
    {
        if (!$this->counties->contains($county)) {
            $this->counties->add($county);
        }
    }

    public function removeCounty(County $county)
    {
        if ($this->counties->contains($county)) {
            $this->counties->removeElement($county);
        }
    }

    public function getCounty(string $code): ?County
    {
        foreach ($this->counties as $county) {
            if ($code === $county->getCode()) {
                return $county;
            }
        }
        return null;
    }

    public function getCounties(): Collection
    {
        return $this->counties;
    }

    public function update(array $data): self
    {
        $this
            ->setName($data['region'])
            ->setCode($data['code_region'])
        ;

        $county = $this->getCounty($data['code_departement']);
        if (null === $county) {
            $county = new County();
        }

        $this->addCounty(
            $county->update($data)
        );

        return $this;
    }
}
