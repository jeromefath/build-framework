<?php

declare(strict_types=1);

namespace Model;

/**
 * Class Income
 *
 * @author Jérôme Fath
 */
class Income implements ModelInterface
{
    /** @var int */
    protected $amount;

    /** @var int */
    protected $year;

    /** @var string */
    protected $type;

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function update(array $data): ModelInterface
    {
        $this
            ->setAmount($data['revenu'])
            ->setYear((int) $data['annee'])
            ->setType($data['type_de_travailleur_independant'])
        ;

        return $this;
    }
}
