<?php

declare(strict_types=1);

namespace Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Class County
 *
 * @author Jérôme Fath
 */
class County implements ModelInterface
{
    /** @var string */
    protected $name;

    /** @var string */
    protected $code;

    /** @var Collection|Income[] */
    protected $incomes;

    public function __construct()
    {
        $this->incomes = new ArrayCollection();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function addIncome(Income $income)
    {
        if (!$this->incomes->contains($income)) {
            $this->incomes->add($income);
        }
    }

    public function removeIncome(Income $income)
    {
        if ($this->incomes->contains($income)) {
            $this->incomes->removeElement($income);
        }
    }

    public function getIncomes(string $type = null): Collection
    {
        $iterator = $this->incomes->getIterator();
        $iterator->uasort(function ($a, $b) {
            return ($a->getYear() < $b->getYear()) ? -1 : 1;
        });

        $incomes = new ArrayCollection(iterator_to_array($iterator));
        if (null === $type) {
            return $incomes;
        }

        return $incomes->filter(function (Income $income) use ($type) {
            return $type === $income->getType();
        });
    }

    public function update(array $data): self
    {
        $this
            ->setName($data['departement'])
            ->setCode($data['code_departement'])
        ;

        $income = new Income();

        $this->addIncome(
            $income->update($data)
        );

        return $this;
    }
}
