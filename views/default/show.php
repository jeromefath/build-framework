<?php $this->layout('layout', ['title' => $title]) ?>

<h1><?= $county->getName() ?> <small>(<?= $county->getCode() ?>)</small></h1>

<h2>Auto-entrepreneur</h2>
<ul>
    <?php foreach ($county->getIncomes('Auto-entrepreneur') as $income) : ?>
    <li>
        <?php echo $income->getYear() ?>
        <?php echo $income->getAmount() ?>
    </li>
    <?php endforeach; ?>
</ul>

<h2>TI classique</h2>
<ul>
    <?php foreach ($county->getIncomes('TI classique') as $income) : ?>
    <li>
        <?php echo $income->getYear() ?>
        <?php echo $income->getAmount() ?>
    </li>
    <?php endforeach; ?>
</ul>

</body>
</html>