<?php $this->layout('layout', ['title' => $title]) ?>

<h1><?= $title ?></h1>

<?php foreach ($areas as $area) : ?>
<article>
    <h2><?= $area->getName() ?></h2>
    <ul>
    <?php foreach ($area->getCounties() as $county): ?>
        <li>
            <a href="show?county=<?= $county->getCode() ?>">
                <?= $county->getName() ?>
            </a>
        </li>
    <?php endforeach; ?>
    </ul>
</article>
<?php endforeach; ?>