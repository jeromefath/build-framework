POO & MVC
=========

Ce projet se déroulera sur **1 semaine** en **binôme**, il doit permettre de découvrir la programmation orientée objet et comprendre l'architecture MVC. Le projet consistera à construire une application (architecture MVC) en assemblant différentes briques logicielles, cette étape permettra de comprendre les fondamentaux en vue de l'utilisation d'un outil comme Symfony.

L'application devra permettre de lister les revenus des indépendants en France : https://www.data.gouv.fr/fr/datasets/les-revenus-des-travailleurs-independants-par-departement/ 

## Objectifs

* Découvrir la programmation orientée objet
* Appronfondir le protocole HTTP (request/response)
* Comprendre l'architecture MVC
* Utiliser un système de routage
* Exploiter un moteur de templates

## Compétences

* Compétence 7 - Développer la partie back-end d'une application

## Contraintes

Votre application devra respecter une architecure MVC et exploiter les briques logicielles listées ci-dessous :

### HTTP (Request/Response)
* https://github.com/laminas/laminas-diactoros/

### Client HTTP
* https://github.com/symfony/http-client

### Router
* https://github.com/thephpleague/route

### Moteur de templates 
* https://github.com/thephpleague/plates
OU
* https://github.com/twigphp/Twig

## Tâches

### Feature 1

1. Définir un controller qui permettra de récupérer les données au format json via le composant HTTPClient

> https://www.data.gouv.fr/fr/datasets/r/8e6efdb1-0152-4b25-a4cf-fdbbfe06a7e0

2. Réorganiser les données via une représentation objet : Région & Département & Revenu

> Vous pouvez utiliser ArrayCollection pour simplifier la gestion des collections d'objets : `composer require doctrine/collections`
> Vous pouvez utiliser la fonction dump() pour simplifier la lecture des objets : `composer require --dev symfony/var-dumper`

3. Afficher l'ensemble des régions et départements en respectant l'arborescence suivante :

* Région A
  * Département 1	
  * Département 2
* Région B
  * Département 3
  * Département 4
  * Département 5

### Feature 2

* Un controller doit permettre de lister par département, les revenus des indépendants des différentes années

> Une url doit permettre d'accéder aux données de la Savoie, une autre url à celles de l'Isère,...
> Si le département n'existe pas, une page d'erreur 404 doit être retournée

## Bonus

* Une page doit permettre de lister par année, les revenus des indépendants des différents départements
* Pour chaque page les données doivent pouvoir être exportées au format CSV
